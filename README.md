# RhoECS (work is in progress)

A simple Entity Component System library implemented using C.

## Getting started

### Add library to the project

 Use CMake and FetchContent. Fetch the library:

  ```cmake
  include(FetchContent)
  FetchContent_Declare(RhoECS
    GIT_REPOSITORY https://gitlab.com/manenko/rho-ecs.git
    GIT_TAG        v0.0.1)
  FetchContent_MakeAvailable(RhoECS)
  ```

  Then link your CMake target with the library:

  ```cmake
  target_link_libraries(${PROJECT_NAME} PRIVATE RhoECS)
  ```

### Optional configuration

You can define the following macros before including the header to change the
library's behavior:

## Example

```c
#include <rho/ecs.h>

int main( void )
{
    return 0;
}
```

## License

MIT
