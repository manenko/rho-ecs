/* rho/ecs.h - v0.0.1 - MIT - Oleksandr Manenko, 2021
 * A simple Entity Component System library written in C.
 *
 *******************************************************************************
 * USAGE
 *
 * #include <rho/ecs.h>
 *
 * int main( void ) {
 *     return 0;
 * }
 *
 *******************************************************************************
 * CONFIGURATION
 *
 * You can define the following macros before including the header to change the
 * library's behavior:
 *
 *******************************************************************************
 * LICENSE
 *
 * Scroll to the end of the file for license information.
 */

#ifndef RHO_ECS_H_INCLUDED
#    define RHO_ECS_H_INCLUDED ( 1 )

#    ifdef __cplusplus
extern "C" {
#    endif

/* Interface ******************************************************************/

#    ifdef __cplusplus
}
#    endif

#endif /* RHO_ECS_H_INCLUDED */

/* Copyright 2021 Oleksandr Manenko
 *
 * Permission is hereby granted, free of  charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction,  including without limitation the rights
 * to use,  copy, modify,  merge, publish,  distribute, sublicense,  and/or sell
 * copies  of the  Software,  and to  permit  persons to  whom  the Software  is
 * furnished to do so, subject to the following conditions:
 *
 * The above  copyright notice and this  permission notice shall be  included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE  IS PROVIDED "AS IS",  WITHOUT WARRANTY OF ANY  KIND, EXPRESS OR
 * IMPLIED,  INCLUDING BUT  NOT LIMITED  TO THE  WARRANTIES OF  MERCHANTABILITY,
 * FITNESS FOR A  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO  EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS  BE LIABLE  FOR ANY  CLAIM,  DAMAGES OR  OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

